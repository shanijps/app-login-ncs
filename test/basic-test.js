/* jshint expr: true */
app = {
  showToast: function(msg) {
    console.log(msg);
  }
};

describe('<app-login>', function() {

  describe('login button', function() {

    var login;

    before(function(done) {
      login = fixture('login1');
      setTimeout(function() {
        // wait for dom-if to get stamped
        done();
      });
    });

    it('should initially be hidden', function() {
      expect(login.$$('#loginButton').disabled).to.be['true'];
    });

    it('should be visible when username & password is entered', function() {
      login.set('username', 'a@a.com');
      login.set('password', 'pass');
      expect(login.$$('#loginButton').disabled).to.be['false'];
    });
  });

  describe('logging in', function() {

    var server;
    var login;

    before(function(done) {
      server = sinon.fakeServer.create({
        autoRespond: true
      });

      server.xhr.useFilters = true;
      server.xhr.addFilter(function(method, url) {
        return url.indexOf('/app', 0) !== 0;
      });

      server.respondWith('POST', '/app/auth/login', function(request) {
        if ('username=a%40a.com&password=pass' === request.requestBody) {
          request.respond(200, {
            'Content-Type': 'application/json'
          }, JSON.stringify({
            username: 'a@a.com',
            name: 'A user'
          }));
        } else {
          request.respond(401, {
            'Content-Type': 'application/json'
          }, JSON.stringify({
            msg: 'BAD_CREDENTIALS'
          }));
        }
      });
      login = fixture('login1');
      setTimeout(function() {
        // wait for dom-if to get stamped
        done();
      });
    });

    it('with invalid credentials should show error toast', function(done) {
      login.set('username', 'a@a.com');
      login.set('password', 'incorrect');
      login.$$('#loginButton').click();

      login.$.login.lastRequest.completes['catch'](function() {
        expect(app.User).to.not.exist;
        done();
      });
    });

    it('with valid credentials should redirect user to app', function(done) {
      login.set('username', 'a@a.com');
      login.set('password', 'pass');
      login.$$('#loginButton').click();

      login.$.login.lastRequest.completes.then(function() {
        expect(app.User).to.exist;
        done();
      });
    });
  });

  describe('remember me functionality', function() {

    describe('when enable-remember-me attribute is not set', function() {

      var login;

      before(function(done) {
        login = fixture('login1');
        setTimeout(function() {
          // wait for dom-if to get stamped
          done();
        });
      });

      it('should not show remember me checkbox', function() {
        expect(login.$$('paper-checkbox')).to.not.exist;
      });

      it('should not send remember-me on submit', function(done) {
        login.$$('#loginButton').click();

        setTimeout(function() {
          expect(login.$.login.body).to.not.match(/(remember-me=)/);

          done();
        });
      });
    });

    describe('when enable-remember-me attribute is set', function() {

      var login;

      before(function(done) {
        login = fixture('login2');
        setTimeout(function() {
          // wait for dom-if to get stamped
          done();
        });
      });

      it('should show remember me checkbox', function() {
        expect(login.$$('paper-checkbox')).to.exist;
      });

      it('should send remember-me=false on submit', function(done) {
        login.$$('#loginButton').click();

        setTimeout(function() {
          expect(login.$.login.body).to.match(/(remember-me=false)/);

          done();
        });
      });

      it('should send remember-me=true on submit when checkbox is checked', function(done) {
        login.set('rememberMe', true);
        login.$$('#loginButton').click();

        setTimeout(function() {
          expect(login.$.login.body).to.match(/(remember-me=true)/);

          done();
        });
      });
    });

  });

  describe('paper-button vs paper-fab', function() {

    describe('when regular-login-button is not set', function() {
      var login;

      before(function(done) {
        login = fixture('login1');
        setTimeout(function() {
          // wait for dom-if to get stamped
          done();
        });
      });

      it('loginButton should be paper-fab', function() {
        expect(login.$$('#loginButton').nodeName).to.equal('PAPER-FAB');
      });

    });

    describe('when regular-login-button is set', function() {
      var login;

      before(function(done) {
        login = fixture('login3');
        setTimeout(function() {
          // wait for dom-if to get stamped
          done();
        });
      });

      it('loginButton should be paper-button', function() {
        expect(login.$$('#loginButton').nodeName).to.equal('PAPER-BUTTON');
      });
    });

  });

});
/* jshint expr: false */
