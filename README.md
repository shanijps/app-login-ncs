# app-login

A web component that provides a basic form login page.
It also uses flytxt-oidc-login if app.oidcAuth is set to true.

Example:

```html
<app-login></app-login>
<!-- The redirect to app only works when adding this element using app-router as follows: -->
<app-route path="/login" element="app-login" bindRouter></app-route>
```