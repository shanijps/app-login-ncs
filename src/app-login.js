Polymer({
  is: 'app-login',

  properties: {

    postLoginResult: {
      type: Object,
      value: {}
    },
    /**
     * The context path for RESTful endpoints, make sure to set app.appContext if this is not set explicitly.
     */
    baseUri: {
      type: String,
      value: function() {
        return app.appContext;
      }
    },

    /**
     * The endpoint for form based login.
     */
    loginUri: {
      type: String,
      value: '/auth/login'
    },

    resetUri: {
      type: String,
      value: '/auth/resetPassword'
    },

    /**
     * Set to true to enable remember me functionality. Shows the remember me checkbox.
     */
    enableRememberMe: {
      type: Boolean,
      value: false
    },

    /**
     * Set to true to enable forgot password functionality. Shows the Forgot Password link.
     */
    enableForgotPassword: {
      type: Boolean,
      value: false
    },

    /**
     * Set to true to render a paper-button instead of paper-fab.
     */
    regularLoginButton: {
      type: Boolean,
      value: false
    },
    
    enableSignUp: {
      type: Boolean,
      value: true
    },
    /**
     * This object is used to obtain user info from flytxt-oidc-login. On successful response, the user object is set to `app.User`.
     */
    user: {
      type: Object,
      observer: '_userChange'
    }
  },

  behaviors: [Flytxt.webL10n],

  attached: function() {
    var me = this;
    me.async(function() {
      me.set('oidcAuth', app.oidcAuth);
      if (app.User) {
        me.redirectToApp();
      }
    });
    if (me.baseURI) {
      var params = me.baseURI.split('?')[1] ? me.baseURI.split('?')[1].split('&') : undefined;
      if (params) {
        if (params[0].split('=')[0] === 'username') {
          me.set('username', params[0].split('=')[1]);
        }
        if (params[1] && params[1].split('=')[0] === 'password') {
          me.set('password', params[1].split('=')[1]);
        }
        if (params[2] && params[2].split('=')[0] === 'forwardUri') {
          me.set('forwardUri', params[2].split('=')[1]);
        }
        if (params[3] && params[3].split('=')[0] === 'accountId') {
          me.set('accountId', params[3].split('=')[1]);
        }
        me.set('rememberMe', false);
        me.handleLogin();
      }
    }
  },

  _userChange: function(user) {
    var me = this;
    if (user) {
      app.User = user;
      me.redirectToApp();
    }
  },

  /**
   * This function is called on-tap of the loginButton.
   */
  handleLogin: function() {
    var me = this;
    var body = 'username=' + encodeURIComponent(me.username) + '&password=' + encodeURIComponent(me.password);
    body = body + '&accountId=' + encodeURIComponent(me.accountId);
    me.$.login.body = body;
    if (me.enableRememberMe) {
      me.$.login.body += '&remember-me=' + encodeURIComponent(me.rememberMe);
    }

    me.$.login.generateRequest();
  },
  /**
   * This function is called on-tap of the register link.
   */
  redirectToSignUp: function() {
    if (this.router) {
      this.router.go('/signup');
    }
  },
  /**
   * Handler for iron-ajax `response` event.
   * 
   * @param {Object} event
   * @param {Object} result
   */
  postLogin: function(event, result) {
    this.postLoginResult = result;
    this.$.getAccountIdsAjax.set('params', {'emailId': this.username , 'accountId': this.accountId});
    this.$.getAccountIdsAjax.generateRequest();
  },
  
  accountIdsAjaxResponse: function(e) {
    if (!e.detail.__data__.response) {
      this.fire('invalid-account', this.postLoginResult);
    } else {
      app.User = this.postLoginResult.response;
      sessionStorage.setItem('accountId', this.accountId);
      this.fire('login-success', this.postLoginResult);
      this.redirectToApp();
    }
  },

  /**
   * Handler for iron-ajax `error` event.
   * 
   * @param {Object} event
   * @param {Object} result
   */
  loginError: function(event, result) {
    if (result.request && 401 === result.request.status) {
      if (this.l10n && this.l10n.loginFailureReasons && this.l10n.loginFailureReasons[result.request.xhr.response.msg]) {
        app.showToast(this.l10n.loginFailureReasons[result.request.xhr.response.msg]);
      } else {
        console.warn('missing localized string for key: ', 'loginFailureReasons.' + result.request.xhr.response.msg);
        app.showToast(result.request.xhr.response.msg);
      }
    }
  },

  /**
   * Computed whether login button should be shown.
   * 
   * @param {String} username
   * @param {String} password
   */
  showLogin: function(username, password, accountId) {
    return !(!username.length || !password.length || !accountId.length);
  },

  /**
   * Redirects the user to the app home page or another deep link from login. Also fires an 'auth-success' event on document object.
   */
  redirectToApp: function() {
     document.dispatchEvent(new CustomEvent('auth-success'));
     if (this.router) {
       this.router.go(app.redirectUri || this.forwardUri || app.get('User.' + this.getAttribute('landing-page-attr')) || '/');
     }
   },

  /**
   * Handler for iron-a11y-keys key-press.
   */
  clickLogin: function() {
    var loginButton = this.$$('#loginButton');
    if (!loginButton.disabled) {
      loginButton.fire('tap');
    }
  },

  onPasswordForgot: function() {
    var me = this;
    if (this.username.length) {
      me.setDialog(true);
      me.$.openDialog.open();
    } else {
      app.showToast(me.webL10n(me.l10n, 'login.noEmail'));
    }
  },

  setDialog: function(open) {
    var me = this;
    if (open) {
      var node = me.$.openDialog;
      var textnode = document.querySelector('body');
      textnode.appendChild(node);
    }
  },

  resetPassword: function() {
    var me = this;
    me.$.reset.body = 'username=' + encodeURIComponent(me.username);
    me.$.reset.generateRequest();
    me.$.openDialog.close();
  },

  postReset: function() {
    var me = this;
    app.showToast(me.webL10n(me.l10n, 'login.resetSuccess'));
  },

  resetError: function() {
    var me = this;
    app.showToast(me.webL10n(me.l10n, 'login.resetFailure'));
  },

  checkCapsLock: function(e) {
    var kc;
    var sk;
    kc = e.keyCode ? e.keyCode : e.which;
    sk = e.shiftKey ? e.shiftKey : ((kc === 16) ? true : false);
    
    if (((kc >= 65 && kc <= 90) && !sk) || ((kc >= 97 && kc <= 122) && sk)) {
      this.$$('#divCapsLockMsg').style.visibility = 'visible';
    } else {
      this.$$('#divCapsLockMsg').style.visibility = 'hidden';
    }
  }
});
